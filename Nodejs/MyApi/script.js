const express = require('express');
const Joi = require('joi'); //used for validation
const mysqlservices = require('./mysqlservices');

const app = express();
app.use(express.json());

const books = [
    { title: 'Harry Potter', id: 1 },
    { title: 'Twilight', id: 2 },
    { title: 'Lorien Legacies', id: 3 }
]

 
app.get('/api/users', (req, res) => {

    mysqlservices.getUsers((users)=>{
        res.send(users);
    })
});

app.get('/api/users/:id', (req, res) => {

    mysqlservices.getUser(req.params.id , (user)=>{
        if (user.length===0) res.status(404).send('<h2 style="font-family: Malgun Gothic; color: darkred;">User not found!</h2>');
        res.send(user);
    })
});


//CREATE Request Handler
app.post('/api/users', (req, res) => {

    const { error } = req.body;
    if (error) {
        res.status(400).send(error.details[0].message)
        return;
    }
    const user = {
        name: req.body.name,
        lastname: req.body.lastname,
        address: req.body.address,
    };
 
    mysqlservices.setUser(user , (user)=>{
         res.send(user);
    });   
});

//UPDATE Request Handler
app.put('/api/users/:id', (req, res) => {
    mysqlservices.getUser(req.params.id , (user)=>{
        if (user.length===0) res.status(404).send('<h2 style="font-family: Malgun Gothic; color: darkred;">User not found!</h2>');
             res.send(user);
        //else
        //mysqlservices.modifyUser ...  
        res.send(user);  
    })
 
    const { error } = req.body;
    if (error) {
        res.status(400).send(error.details[0].message);
        return;
    }
});

//DELETE Request Handler
app.delete('/api/users/:id', (req, res) => {

    mysqlservices.getUser(req.params.id , (user)=>{
        if (user.length===0) res.status(404).send('<h2 style="font-family: Malgun Gothic; color: darkred;">User not found!</h2>');
             res.send(user);
        //else
        //mysqlservices.deleteUser ...  
        res.send(user);  
    })
});



//PORT ENVIRONMENT VARIABLE
const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`Listening on port ${port}..`));


