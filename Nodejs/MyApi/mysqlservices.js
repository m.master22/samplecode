
const mysql=require("mysql");
var json = require('./config.json')

function getMysqlConnection (){
    const con= mysql.createConnection({

        host: json.mysql.host,
        user: json.mysql.user,
        password: json.mysql.password,
        database: json.mysql.database

    });
    con.connect((err) => {
        if (err) throw err;
        console.log('Connected!');
    });
    return con;
}

function getUsers (callback){
 
    const con = getMysqlConnection();

    con.query('SELECT * FROM users', (err,rows) => {
        if(err) throw err;
        callback(rows);  
    });
}

function getUser (userId , callback){
 
    const con = getMysqlConnection();

    con.query(`SELECT * FROM users where Id = ${userId}`, (err,rows) => {
        if(err) throw err;
        callback(rows);  
    });
}

function setUser (user , callback){
 
    const con = getMysqlConnection();

    con.query(`SELECT max(id) maxId FROM users`, (err,rows) => {
        if(err) throw err;
        const newId = rows[0].maxId + 1;
        user.id = newId;
        con.query(`insert into users (id,name , lastname , address) values (${newId}, '${user.name}', '${user.lastname}', '${user.address}')`, (err,rows) => {
            if(err) throw err;
            callback(user);  
        });
    });
}

exports.getUsers = getUsers;
exports.getUser = getUser;
exports.setUser = setUser;
