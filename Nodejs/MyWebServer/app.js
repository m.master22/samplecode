import {createServer} from 'http';
import {readFile} from 'fs';
import path from 'path';
import {fileURLToPath} from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
 
 
const server = createServer(
    (req,res)=>{

        let filePath = path.join(
            __dirname,
            'public',
            req.url	=== '/' ? 'index.html' : req.url
        );
        let extname = path.extname(filePath)

        
        let contentType = 'text/html';
        switch(extname){
            case ".js":
                contentType  = "text/javascript";
                break;
            case ".css":
                contentType  = "text/css";
                break;
            case ".json":
                contentType  = "application/json";
                break;
            case ".png":
                contentType  = "image/png";
                break;
            case ".jpg":
                contentType  = "image/jpg";
                break;
        }

        if(contentType === 'text/html' && extname === ''){
            filePath +='.html';
        }


        readFile(
            filePath ,
            (err , data)=>{

                if(err) {

                    if (err.code === "ENOENT") {
                        readFile(path.join(__dirname, "public", "404.html"), (err, data) => {
                            res.writeHead(404, {"Content-Type": "text/html"})
                            res.end(data, "utf8")
                        });
                    }else{
                        res.writeHead(500)
                        res.end(`Server Error : ${err.code}`)
                    }
                } else {
                    res.writeHead(200,{'Content-Type':contentType})
                    res.end(data, 'utf8')
                }
            });

    }
)

server.listen(3000 , ()=> console.log('server running on port 3000'))
  