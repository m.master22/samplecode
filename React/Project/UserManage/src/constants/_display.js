const display = {};

const Display = [
    {
        id: "0",
    },
    {
        id: "1",
    },

].map((item) => ({ ...item, name: `display.${item.id}` }));

display.Display = Display;

export default display;
