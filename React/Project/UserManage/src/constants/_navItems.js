const navItems = [
  {
    pathname: "/",
    icon: "house-door",
  },
  {
    pathname: "user-list",
    icon: "person",
  }
].map((item, index) => ({ ...item, title: `nav-items.${index}`, id: index }));
export default navItems;
