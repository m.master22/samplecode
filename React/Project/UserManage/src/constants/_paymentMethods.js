const paymentMethods = [
  {
    id: "0",
    image: "/icons/payment-methods/Payoneer.svg",
  },
  {
    id: "1",
    image: "/icons/payment-methods/BankTransfer.svg",
  },
  {
    id: "2",
    image: "/icons/payment-methods/Wise.svg",
  },
  {
    id: "3",
    image: "/icons/payment-methods/Stripe.svg",
  },
  {
    id: "4",
    image: "/icons/payment-methods/Crypto.svg",
  },
].map((item) => ({
  ...item,
  name: `payment-methods.${item.id}`,
}));
export default paymentMethods;
