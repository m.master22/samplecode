import { Col, Row } from "react-bootstrap";
import "./index.scss";
import {axios} from "../../boot";
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import Chart from 'chart.js/auto';
import { Line } from 'react-chartjs-2';
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router";

export default function Home() {
  const todoItems = [
    {
      title: "Personal",
      color: "info",
      bodyText: "Rework Back-End API Collection",
    },
    {
      title: "Done",
      color: "success",
      bodyText: "Doctors Appointment",
    },
    {
      title: "In proccess",
      color: "warning",
      bodyText: "Setup the Front-End Framework",
    },
  ];
  const [data, setData] = useState({});
  const [chartData, setChartData] = useState({});
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const chartConfig = {
    labels: chartData.chart_labels,
    datasets: [
      {
        label: "Top Up amounts",
        data: chartData.chart_value,
        fill: true,
        backgroundColor: "rgba(75,192,192,0.2)",
        borderColor: "rgba(75,192,192,1)"
      },
    ]
  };
  
    const getChartData = () => {
    const url = "/v1/web_template/getWeb_Admin_MainChart";
    const body = "";
    axios.post(url, body).then(({ data }) => {
      const list = data?.data ?? {};
      const chart_labels = list.map(function(item){return item.chart_label});
      const chart_value = list.map(function(item){return item.chart_value});
      setChartData({chart_labels,chart_value})
    });
  }
  
  const getData = () => {
    const url = "/v1/web_template/getWeb_Admin_Template_Info";
    const body = "";
    axios.post(url, body).then(({ data }) => {
      setData(data?.data ?? {});
    });
  };

  const login_fake = () => {
   // dispatch({ type: "SET_IS_LOGGED", data });
    //navigate("/");
  }

  useEffect(()=> {
 //   getData();
  //  getChartData();
  },[])

  return (
      <div>
        <div className='text-start m-auto w-fit pt-5'>
          <p>
            This sample
            <span className='ms-1 me-1 fw-bold'>React.js</span>
            application developed by
            <span className='ms-1 fw-bold'>Mojtaba Ghasemi</span>
          </p>
          <p>
            <span className='me-1 fw-bold'>linkedin url:</span>
             https://www.linkedin.com/in/mojtaba-ghasemi-934a0477/
          </p>
          <p>
            <span className='me-1 fw-bold'>Email:</span>
            mojtaba_ghasemi@outlook.com
          </p>
        </div>
      </div>
  );
}
