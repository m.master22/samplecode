import React, { useEffect, useState } from 'react';
import {
    FilterSection,
    Pagination,
    Table,
    Text,
} from '../../components';
import { axios } from '../../boot';
import { t } from 'i18next';
import ManageUser from "./ManageUser";

export default function UserList() {

    const [data, setData] = useState([]);
    const [manageUserInfo, setManageUserInfo] = useState({status:false});

    const formControls = [
        {
            state: 'Name',
        },
        {
            state: 'LastName',
        },
    ].map((item, index) => ({
        ...item,
        rules: [],
        label: t(`user-list.filter-inputs.${index}`),
    }));


    const [gridInfo, setGridInfo] = useState({
        filterData: {},
        activePage: 1,
        totalRecordCount:0,
        pageSize:5,
        maxPageSize:0,
    });

    const getData = (e) => {

        const url = '/RESTfullAPI/singletonWebService';
        const body = new FormData();
        body.set('serviceName', 'Z_Smpl_Users_List');
        body.set('userToken', '');
        body.set(
            'inputParams',
            JSON.stringify({
                PageNum: gridInfo.activePage,
                PageSize:gridInfo.pageSize,
                ...gridInfo.filterData
            })
        );

        axios.post(url, body).then(({ data }) => {
            if (data.IsSuccessful) {
                setData(data?.List1 ?? []);
                setGridInfo({
                                    ...gridInfo,
                                    totalRecordCount: data?.TotalRecordCount,
                                    maxPageSize:data?.MaxPageSize})
            }
        });
    };

    const manageUser = (type , id) => {
        if(type === 'delete'){
            const url = '/RESTfullAPI/singletonWebService';
            const body = new FormData();
            body.set('serviceName', 'Z_Smpl_Users_CRUD');
            body.set('userToken', '');
            body.set(
                'inputParams',
                JSON.stringify({
                    Id: id,
                    CommandType:type
                })
            );
            axios.post(url, body).then(({ data }) => {
                if (data.IsSuccessful) {
                    getData();
                }
            });

        }
    }


    useEffect(getData, []);

    return (
        <div className="TopUpRequests">
            <FilterSection
                formControls={formControls}
                title="user-list.filter-title"
                onSubmit={(e) => {
                    gridInfo.activePage = 1;
                    gridInfo.filterData = e;
                    getData(e);
                }}
                showFilterLabel
            >

                <i className="bi bi-plus-square-dotted fs-2 float-end cursor-pointer text-success"
                    onClick={()=> setManageUserInfo({...manageUserInfo, status: true})}></i>

                <Table className="layout">
                    <thead>
                    <tr>
                        <th>
                            <Text value="user-list.table-headers.0" />
                        </th>
                        <th>
                            <Text value="user-list.table-headers.1" />
                        </th>
                        <th>
                            <Text value="user-list.table-headers.2" />
                        </th>
                        <th>
                            <Text value="user-list.table-headers.3" />
                        </th>
                        <th>
                            <Text value="user-list.table-headers.4" />
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((item, index) => (
                        <tr key={index}>
                            <td>{item.Id}</td>
                            <td>{item.Name}</td>
                            <td>{item.LastName}</td>
                            <td>{item.Tel}</td>
                            <td style={{width:"70px"}}><i className="bi bi-trash text-danger"
                                                          onClick={()=>manageUser('delete' ,item.Id )}
                                                        ></i></td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
                <Pagination
                    activePage={gridInfo.activePage}
                    setActivePage={(e) => {
                        gridInfo.activePage = e;
                        getData();
                    }}
                    totalPages={(gridInfo.pageSize<gridInfo.maxPageSize)?
                        gridInfo.totalRecordCount/gridInfo.pageSize
                        :gridInfo.totalRecordCount/gridInfo.maxPageSize}
                />
            </FilterSection>

            <ManageUser
                show={manageUserInfo.status}
                onHide={() => setManageUserInfo({...manageUserInfo , status: false})}
                afterApprove={() => getData()}
            />

        </div>
    );
}
