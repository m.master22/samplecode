import React, { useState, useEffect } from "react";
import {Col, Row} from "react-bootstrap";
import { axios } from "../../boot";
import {
    Button,
    Form,
    Input,
    Modal,
    Text,
    Dropdown
} from "../../components";
import { countryCodes, rules } from "../../constants";


import { t } from "i18next";

export default function ManageUser({ show = true, onHide = () => {} ,afterApprove = () => {}}) {
    const [data, setData] = useState({
         countryCode: "+49",
    });
    const _defaultData = {countryCode: "+49"}
    const formControls = [
        {
            title: t("manage-user.inputs.0"),
            state: "Name",
            rules: rules.required,
        },
        {
            title: t("manage-user.inputs.1"),
            state: "LastName",
            rules: rules.required,
        },
        {
            title: t("manage-user.inputs.2"),
            state: "Tel",
            type: "number",
            rules: [
                ...rules.required,
                (val = "") => val.length <= 12 || "Maximum 12 characters",
            ],
            prepend: (
                <Dropdown
                    outline
                    filter
                    variant="dark"
                    drop="up"
                    label={data.countryCode}
                    value={data.countryCode}
                    setValue={(val) => setData((p) => ({ ...p, countryCode: val }))}
                    items={countryCodes}
                />
            ),
        },


    ];



    const submitAccountInfo = () => {
        const url = '/RESTfullAPI/singletonWebService';
        data.Tel = `${data.countryCode}-${data.Tel}`;
        const body = new FormData();
        body.set('serviceName', 'Z_Smpl_Users_CRUD');
        body.set('userToken', '');
        body.set(
            'inputParams',
            JSON.stringify({
                ...data,
                CommandType:'insert'
            })
        );


        axios.post(url, body).then(() => {
            onHide(false);
            afterApprove();
        });
    };

    const init = () => {
        setData(_defaultData);
    }

    useEffect(init , [show]);

    return (
        <React.Fragment>
            <Modal show={show} onHide={onHide}>

                <Form className="row p-5" onSubmit={submitAccountInfo}>
                    <h5 className="col-12 text-start">
                        <Text value="manage-user.title" />
                    </h5>
                    {formControls.map((item, index) => (
                        <Col key={index} xs="12">
                            <Row className="row-gap-1">
                                <Col xs="12" md="3" className="text-start fs-7 px-3 px-md-0">
                                    <span className='fs-6'>
                                        {item.title}
                                    </span>

                                    <span className="text-danger">
                    {item.rules?.length > 0 && "*"}
                  </span>
                                </Col>
                                <Col xs="12" md="9" className="mt-2">
                                    {React.createElement(item.tag ?? Input, {
                                        ...item,
                                        value: data[item.state],
                                        setValue: (val) =>
                                            setData((p) => ({ ...p, [item.state]: val })),
                                    })}
                                </Col>
                            </Row>
                        </Col>
                    ))}
                    <Col xs="12" className="row justify-content-end col-gap-2 px-4">
                        <Button className="col-4" type="submit">
                            <Text value="general.save" />
                        </Button>
                    </Col>
                </Form>
            </Modal>
        </React.Fragment>
    );
}
