import { useRef } from "react";
import { Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import calendarIcon from "../../assets/icons/calendar.svg";
import { Button, Input, Text } from "../../components";
import { axios } from "../../boot";
import { toast, logout } from "../../methods";
export default function Profile() {
  const profile = useSelector((s) => s.profile);
  const formControls = [
    {
      state: "firstName",
    },
    {
      state: "lastName",
    },
    {
      state: "email",
    },
    {
      state: "profession",
    },
    {
      state: "bio",
      as: "textarea",
    },
  ].map((item, index) => ({
    ...item,
    readOnly: true,
    key: index,
    label: `profile.inputs.${index}`,
  }));
  const inputFile = useRef();
  const dispatch = useDispatch();

  const setProfileImage = (data) => {
    dispatch({ type: "SET_PROFILE_IMAGE", data });
  };

  const toBase64 = (file) => {
    const reader = new FileReader();
    reader.onload = () => {
      setProfileImage(reader.result);
      const text = "messages.200";
      toast({ text });
    };
    reader.readAsDataURL(file);
  };

  const submitChangeProfile = (e) => {
    const file = e.target.files[0];
    if (file) {
      if (file.size <= 2000000) {
        const url = "/v1/user-accounts/UploadProfilePhoto";
        const body = new FormData();
        body.append("DocFile", file);
        axios.post(url, body).then(() => {
          toBase64(file);
        });
      } else {
        const text = "messages.large-file";
        toast({ text, type: "error" });
      }
    }
  };
  return (
    <div className="Profile">
      <h1 className="w-fit d-flex flex-center h3">
        <i className="bi bi-gear me-2" />
        <Text value="profile.h1" />
      </h1>
      <div className="d-flex flex-center w-fit mt-4">
        <div className="profile-img-section bg-white shadow p-2 me-4">
          {profile.image ? (
            <img src={profile.image} alt="profile" />
          ) : (
            <i className="bi bi-person w-100 h-100 d-flex flex-center text-dark" />
          )}
        </div>
        <div className="d-flex flex-column row-gap-3">
          <Button
            variant="info"
            className="text-white"
            label="profile.change-picture"
            onClick={() => inputFile.current.click()}
          />
          <input
            ref={inputFile}
            onChange={submitChangeProfile}
            type="file"
            accept=".jpg, .jpeg, .png"
            className="d-none"
          />
          <Button
            outline
            variant="dark"
            label="profile.delete-picture"
            onClick={() => {
              // const url = "/v1/user-accounts/UploadProfilePhoto";
              // const body = new FormData();
              // body.append("DocFile", " - ");
              // axios.post(url, body).then(() => {});
            }}
          />
        </div>
      </div>
      <Row className="align-items-start mt-5">
        <Col xs="12" lg="7" xl="8" className="px-0">
          <Row>
            {formControls.map((item) => (
              <Col key={item.key} xs="12" lg={item.key === 4 ? "12" : "6"}>
                <Input {...item} value={profile[item.state]} />
              </Col>
            ))}
          </Row>
        </Col>
        <Col xs="12" lg="5" xl="4">
          <div
            style={{ marginTop: "35px", padding: "4rem 0" }}
            className="d-flex flex-column flex-center bg-dark bg-opacity-25 px-2 rounded"
          >
            <h5 className="white-space-pre-wrap text-white text-center fw-bold mb-4">
              <Text value="profile.calendar" />
            </h5>
            <img width="100" src={calendarIcon} alt="google-calendar" />
          </div>
        </Col>
      </Row>
      <button
        onClick={logout}
        className="reset d-block ms-auto text-dark-gray mt-5 fs-5"
      >
        <Text value="profile.logout" />
      </button>
    </div>
  );
}
