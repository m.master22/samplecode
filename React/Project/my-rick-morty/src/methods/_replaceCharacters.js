export default function replaceCharacters(val = "") {
  const reg = /[&\/\\#,+()$~%.'":*?<>{}]/g;
  return val.replace(reg, "");
}
