export const getQueryStringParameterByName = (name, url=window.location.href) => {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


export const formatNumber = (val) => {
    // format number 1000000 to 1,234,567
    if(val != undefined)
        return val.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    else
        return '';
}

export const deFormatNumber = (val) => {
    // format number  1,234,567 to 1000000
    if(val != undefined)
        return val.toString().replace(",", "");
    else
        return '';
}

export const IsNullOrEmpty = (val) => {
    if(val === undefined || val === '' || val === null)
        return true;
    else
        return false;
}