const gender = {};

const Gender = [
    {
        id: "female",
        name:`rick-morty.gender.female`,
    },
    {
        id: "male",
        name:`rick-morty.gender.male`,
    },
    {
        id: "genderless",
        name:`rick-morty.gender.genderless`,
    },
    {
        id: "unknown",
        name:`rick-morty.gender.unknown`,
    },
]
//.map((item) => ({ ...item, name: `rick-morty.status.${item.id}` }));

gender.Gender = Gender;

export default gender;
