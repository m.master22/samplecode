const species = {};

const Species = [
    {
        id: "Human",
        name:`rick-morty.Species.human`,
    },
    {
        id: "Alien",
        name:`rick-morty.Species.alien`,
    },
    {
        id: "Humanoid",
        name:`rick-morty.Species.humanoid`,
    },
    {
        id: "Poopybutthole",
        name:`rick-morty.Species.poopybutthole`,
    },
    {
        id: "Mythological",
        name:`rick-morty.Species.mythological`,
    },
    {
        id: "Unknown",
        name:`rick-morty.Species.unknown`,
    },
    {
        id: "Animal",
        name:`rick-morty.Species.animal`,
    },
    {
        id: "Disease",
        name:`rick-morty.Species.disease`,
    },
    {
        id: "Robot",
        name:`rick-morty.Species.robot`,
    },
    {
        id: "Cronenberg",
        name:`rick-morty.Species.cronenberg`,
    },
    {
        id: "Planet",
        name:`rick-morty.Species.planet`,
    },
]

species.Species = Species;

export default species;
