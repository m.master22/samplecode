const status = {};

const Status = [
  {
    id: "Alive",
    name:`rick-morty.status.alive`,
  },
  {
    id: "Dead",
    name:`rick-morty.status.dead`,
  },
  {
    id: "Unknown",
    name:`rick-morty.status.unknown`,
  },
]
    //.map((item) => ({ ...item, name: `rick-morty.status.${item.id}` }));

status.Status = Status;

export default status;
