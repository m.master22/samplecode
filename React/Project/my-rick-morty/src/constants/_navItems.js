const navItems = [
  {
    pathname: "/",
    icon: "house-door",
  },
  {
    pathname: "character-rest-list",
    icon: "person",
  },
  {
    pathname: "character-graphql-list",
    icon: "person",
  }
].map((item, index) => ({ ...item, title: `nav-items.${index}`, id: index }));
export default navItems;
