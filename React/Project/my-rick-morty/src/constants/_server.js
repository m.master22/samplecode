
const server = {
    Rest:"https://rickandmortyapi.com",
    GraphQl:"https://rickandmortyapi.graphcdn.app/"
};

export default server;

