import { Navigate } from "react-router";
import MainLayout from "../layouts/MainLayout";
import Home from "../pages/Home";
import CharactersList_Rest from "../pages/CharactersList_Rest";
import CharactersList_Graphql from "../pages/CharactersList_Graphql";

const routes = ({ isLogged = false }) => [
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        path: "",
        element: <Home />,
      },
      {
        path: "character-graphql-list",
        element: <CharactersList_Graphql/>,
      },
      {
        path: "character-rest-list",
        element: <CharactersList_Rest/>,
      },
    ],
  },
  {
    path: "*",
    element: <h1 className="text-center py-5">404</h1>,
  },
];
export default routes;
