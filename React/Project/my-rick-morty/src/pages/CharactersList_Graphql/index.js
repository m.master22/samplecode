import React, { useEffect, useState } from 'react';
import {
    FilterSection,
    Pagination,
    Table,
    Text,
    Select
} from '../../components';
import { status,species,gender } from "../../constants";
import { t } from 'i18next';
import moment from "moment";
import { useQuery, gql } from "@apollo/client";
import Spinner from 'react-bootstrap/Spinner';
import CharacterDetailModal from "../CharacterDetailModal";

export default function CharactersList_Graphql() {

    const [manageCharInfo, setManageCharInfo] = useState({status:false});

    const formControls = [
        {
            state: 'name',
        },
        {
            tag: Select,
            state: "status",
            items: status.Status,
        },
        {
            tag: Select,
            state: "gender",
            items: gender.Gender,
        },
        {
            tag: Select,
            state: "species",
            items: species.Species,
        },
    ].map((item, index) => ({
        ...item,
        rules: [],
        label: t(`character-list.filter-inputs.${index}`),
    }));


    const [gridInfo, setGridInfo] = useState({
        filterData: {},
        activePage: 1,
        totalRecordCount:0,
        pageSize:5,
        maxPageSize:0,
    });


    const query = gql`
          query Character($page: Int, $name:String, $status:String, $gender:String, $species:String)  
          {
              characters(page: $page, filter: { name: $name , status:$status, gender:$gender , species:$species }) {
                info {
                  count,pages
                }
                 results {
                  id,name,status,species,gender,created,image
                }
              }
          }
        `;

    const { loading, error, data } = useQuery(query, {
        variables: { page: gridInfo.activePage ,
                     name: gridInfo.filterData.name,
                     status: gridInfo.filterData.status,
                     gender: gridInfo.filterData.gender,
                     species: gridInfo.filterData.species
        }
    })



    const getStatusColor = (status) => {
        if(status === 'Alive')
            return 'text-success'
        if(status === 'Dead')
            return 'text-danger'

    }

    return (
        <div className="CharactersList">
            <FilterSection
                formControls={formControls}
                title="character-list.filter-title"
                onSubmit={(e) => {
                    setGridInfo({...gridInfo , activePage: 1, filterData:e})
                }}
                showFilterLabel
            >

                {
                    (loading)&&
                    <div className="d-inline-flex w-100">
                        <Spinner animation="border" variant="primary" className="m-auto"/>
                    </div>
                }

                {
                    (!loading)&&
                         <Table className="layout">
                        <thead>
                        <tr>
                            <th>
                                <Text value="character-list.table-headers.0" />
                            </th>
                            <th>
                                <Text value="character-list.table-headers.1" />
                            </th>
                            <th>
                                <Text value="character-list.table-headers.2" />
                            </th>
                            <th>
                                <Text value="character-list.table-headers.3" />
                            </th>
                            <th>
                                <Text value="character-list.table-headers.4" />
                            </th>
                            <th>
                                <Text value="character-list.table-headers.5" />
                            </th>
                            <th>
                                <Text value="character-list.table-headers.6" />
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {data?.characters?.results.map((item, index) => (
                            <tr key={index}>
                                <td>
                                    <i className="bi bi-eye-fill fs-2 cursor-pointer text-info"
                                       onClick={()=> setManageCharInfo(
                                           {...manageCharInfo, status: true, selectedId:item.id})}></i>
                                </td>
                                <td >{item.name}</td>
                                <td className={getStatusColor(item.status)}>{item.status}</td>
                                <td>{item.species}</td>
                                <td>{item.gender}</td>
                                <td>{moment(item.created)
                                    .local("en")
                                    .format("YYYY/MM/DD")}</td>
                                <td>
                                    <img src={item.image} width='60px' height='60px'/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                }

                <Pagination
                    activePage={gridInfo.activePage}
                    setActivePage={(e) => {
                        setGridInfo({...gridInfo , activePage: e})
                    }}
                    totalPages={data?.characters?.info.pages}
                />
            </FilterSection>

            <CharacterDetailModal
                show={manageCharInfo.status}
                selectedId={manageCharInfo.selectedId}
                onHide={() => setManageCharInfo({...manageCharInfo , status: false})}
                //afterApprove={() => getData()} // if modal change record, we can use this line
            />
        </div>
    );
}
