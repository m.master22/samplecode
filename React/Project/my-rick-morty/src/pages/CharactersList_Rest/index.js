import React, { useEffect, useState } from 'react';
import {
    FilterSection,
    Pagination,
    Table,
    Text,
    Select
} from '../../components';
import { status,species,gender } from "../../constants";
import { axios } from '../../boot';
import { t } from 'i18next';
import moment from "moment";
import CharacterDetailModal from "../CharacterDetailModal";

export default function CharactersList_Rest() {

    const [data, setData] = useState([]);
    const [manageCharInfo, setManageCharInfo] = useState({status:false});

    const formControls = [
        {
            state: 'name',
        },
        {
            tag: Select,
            state: "status",
            items: status.Status,
        },
        {
            tag: Select,
            state: "gender",
            items: gender.Gender,
        },
        {
            tag: Select,
            state: "species",
            items: species.Species,
        },
    ].map((item, index) => ({
        ...item,
        rules: [],
        label: t(`character-list.filter-inputs.${index}`),
    }));

    const [gridInfo, setGridInfo] = useState({
        filterData: {},
        activePage: 1,
        totalRecordCount:0,
        pageSize:5,
        maxPageSize:0,
    });

    const  getData = (e) => {

        const url = `/api/character/?page=${
            gridInfo.activePage}&name=${
            gridInfo.filterData.name??''}&status=${
            gridInfo.filterData.status??''}&gender=${
            gridInfo.filterData.gender??''}&species=${gridInfo.filterData.species??''}`;


        axios.get(url).then(({ data }) => {
            gridInfo.totalRecordCount = data.info.pages;
            setData(data.results)
        });
    };


    const getStatusColor = (status) => {
        if(status === 'Alive')
            return 'text-success'
        if(status === 'Dead')
            return 'text-danger'

    }

    useEffect(getData, []);

    return (
        <div className="CharactersList">

            <FilterSection
                formControls={formControls}
                title="character-list.filter-title"
                onSubmit={(e) => {
                    gridInfo.activePage=1
                    gridInfo.filterData=e
                    getData(e);
                }}
                showFilterLabel
            >



                <Table className="layout">
                    <thead>
                    <tr>
                        <th>
                            <Text value="character-list.table-headers.0" />
                        </th>
                        <th>
                            <Text value="character-list.table-headers.1" />
                        </th>
                        <th>
                            <Text value="character-list.table-headers.2" />
                        </th>
                        <th>
                            <Text value="character-list.table-headers.3" />
                        </th>
                        <th>
                            <Text value="character-list.table-headers.4" />
                        </th>
                        <th>
                            <Text value="character-list.table-headers.5" />
                        </th>
                        <th>
                            <Text value="character-list.table-headers.6" />
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    {data.map((item, index) => (
                        <tr key={index}>
                            <td>
                                <i className="bi bi-eye-fill fs-2 cursor-pointer text-info"
                                   onClick={()=> setManageCharInfo(
                                        {...manageCharInfo, status: true, selectedId:item.id})}></i>
                            </td>
                            <td >{item.name}</td>
                            <td className={getStatusColor(item.status)}>{item.status}</td>
                            <td>{item.species}</td>
                            <td>{item.gender}</td>
                            <td>{moment(item.created)
                                .local("en")
                                .format("YYYY/MM/DD")}</td>
                            <td>
                                <img src={item.image} width='60px' height='60px'/>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
                <Pagination
                    activePage={gridInfo.activePage}
                    setActivePage={(e) => {
                        gridInfo.activePage = e;
                        getData();
                    }}
                    totalPages={gridInfo.totalRecordCount}
                />
            </FilterSection>

            <CharacterDetailModal
                show={manageCharInfo.status}
                selectedId={manageCharInfo.selectedId}
                onHide={() => setManageCharInfo({...manageCharInfo , status: false})}
                //afterApprove={() => getData()} // if modal change record, we can use this line
            />

        </div>
    );
}
