import React, { useState, useEffect } from "react";
import {Col, Row} from "react-bootstrap";
import { axios } from "../../boot";
import {
    Button,
    Form,
    Input,
    Modal,
    Text,
    Dropdown
} from "../../components";
import { get } from 'lodash';
import { countryCodes, rules } from "../../constants";


import { t } from "i18next";
import Spinner from "react-bootstrap/Spinner";

export default function CharacterDetailModal({ show = true, onHide = () => {}
                                                ,afterApprove = () => {} , selectedId=0}) {
    const [data, setData] = useState();
    const [loading, setLoading] = useState(false);
    const formControls = [
        {
            title: t("manage-character.inputs.0"),
            state: "status",
        },
        {
            title: t("manage-character.inputs.1"),
            state: "gender",
        },
        {
            title: t("manage-character.inputs.2"),
            state: "species",
        },
        {
            title: t("manage-character.inputs.3"),
            state: "location.name",
        },
        {
            title: t("manage-character.inputs.4"),
            state: "origin.name",
        }
    ];


    const getData = () => {

        const url = `/api/character/${selectedId}`;
        setLoading(true);
        axios.get(url).then(({data}) => {
            setLoading(false);
            setData(data);
        });
    };

    const init = () => {
        if (show)
            getData();
    }

    useEffect(init, [show]);

    return (
        <React.Fragment>
            <Modal show={show} onHide={onHide}>

                {
                    (show && data && !loading) &&

                    <Form className="p-4">
                        <Row>
                            <Col xs={12} className='fs-3 fw-bold text-center mb-3'>{data.name}</Col>
                        </Row>
                        <Row>
                            <Col xs={12} className='mb-3  text-center '>
                                <img src={data.image} width='300px' height='300px'/>
                            </Col>
                        </Row>

                          {formControls.map((item, index) => (
                              <Row className='mb-2 text-start' key={index}>
                                  <Col xs={12}  >
                                      <span className='fw-bold me-2'>{item.title}:</span>
                                      <span className=''>{get(data,item.state)}</span>
                                  </Col>

                              </Row>
                          ))}
                    </Form>
                }
            </Modal>
        </React.Fragment>
    );
}
