import React, { useLayoutEffect, useRef, useState } from "react";
import { Col, FormControl, InputGroup, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink, Outlet, useLocation } from "react-router-dom";
import logo from "../../assets/logos/React-icon.svg";
import { navItems } from "../../constants";
import { Text } from "../../components";
import { axios } from "../../boot";
import "./index.scss";
export default function MainLayout() {
  const sidebar = useRef();
  const dispatch = useDispatch();
  const location = useLocation();
  const profile = useSelector((s) => s.profile);

  const [showSidebar, setShowSidebar] = useState(true);

  const setProfile = (data) => {
    dispatch({ type: "SET_PROFILE", data });
  };
  const setProfileImage = (data) => {
    dispatch({ type: "SET_PROFILE_IMAGE", data });
  };
  const handleChangeLocation = () => {
    setShowSidebar(false);
  };
  const handleShowSidebar = () => {
    sidebar.current.classList.toggle("active", showSidebar);
  };

  const getProfileImage = () => {
    const url = "/v1/user-accounts/DownloadDirectlyProfilePhoto";
    const body = "";
    const option = { responseType: "blob" };
    axios.post(url, body, option).then(({ data }) => {
      if (data?.type !== "application/json" && data?.type !== "text/xml") {
        const url = window.URL.createObjectURL(new Blob([data]));
        setProfileImage(url);
      } else {
        setProfileImage(undefined);
      }
    });
  };

  const getProfile = () => {
    const url = "/v1/user-accounts/GetProfile";
    const body = "";
    axios.post(url, body).then(({ data }) => {
      setProfile(data.data);
      getProfileImage();
    });
  };

  useLayoutEffect(handleShowSidebar, [showSidebar]);
  useLayoutEffect(handleChangeLocation, [location.pathname]);
  // useLayoutEffect(getProfile, []);
  return (
    <Row className="MainLayout align-items-start overflow-hidden">
      <Col ref={sidebar} xs="12" lg="3" xl="2" className="sidebar-section p-0">
        <div className="sidebar bg-dark p-4 position-relative">
          <div
            className="toggle position-absolute display-5"
            onClick={() => setShowSidebar((p) => !p)}
          >
            <i className="light d-block text-light bi bi-triangle-fill" />
            <i className="dark d-block text-dark bi bi-triangle-fill position-absolute top-0 left-0" />
          </div>
          <div>
            <span className='text-info'>Rick&Morty Admin Panel</span>
            <img className="w-100 mb-5 mt-5" src={logo} alt="logo" />
            <div className="nav-items">
              {navItems.map((item) => (
                <NavLink
                  key={item.id}
                  to={item.pathname}
                  className={({ isActive }) =>
                    `d-flex align-items-center w-100 text-white py-2 col-gap-2 border transition ${
                      isActive
                        ? "active border-danger"
                        : "border-white opacity-5"
                    }`
                  }
                >
                  <i className={`bi bi-${item.icon}`} />
                  <Text value={item.title} />
                </NavLink>
              ))}
            </div>
          </div>
        </div>
      </Col>
      <Col xs="12" lg="9" xl="10" className="main-section">
        <header className="row w-100 p-3 d-flex flex-column-reverse flex-lg-row align-items-center justify-content-between">
          <Col xs="12" lg="6">
            <InputGroup className="rounded overflow-hidden shadow-sm">
              <InputGroup.Text className="bg-white border-0">
                <i className="bi bi-search" />
              </InputGroup.Text>
              <FormControl
                className="shadow-none border-0"
                placeholder="Search..."
              />
            </InputGroup>
          </Col>
          <Col xs="12" lg="6">
            <div className="w-fit d-flex flex-center col-gap-3 mx-auto me-lg-0">
              {/*<i className="fs-4 bi bi-bell"></i>*/}
              <div>
                <h6 className="fw-bold">{profile.fullName}</h6>
                <p className="text-secondary">{profile.profession}</p>
              </div>
              <Link
                to="/profile"
                style={{ width: "75px", height: "75px" }}
                className="profile-img-section position-relative bg-dark rounded-circle"
              >
                {profile.image ? (
                  <img src={profile.image} alt="profile" />
                ) : (
                  <i className="bi bi-person w-100 h-100 d-flex flex-center text-white fs-1" />
                )}
                <i
                  to="/profile"
                  className="bi bi-gear position-absolute d-flex flex-center bottom-0 left-0 bg-dark text-white rounded-circle fs-6"
                />
              </Link>
            </div>
          </Col>
        </header>
        <main className="px-3 pb-5">
          <Outlet />
        </main>
      </Col>
    </Row>
  );
}
