import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import store from "./redux";
import { BrowserRouter } from "react-router-dom";
import "./scss/index.scss";
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client";
import {server} from "./constants";

const client = new ApolloClient({
    uri: server.GraphQl,
    cache: new InMemoryCache()
});

ReactDOM.render(

  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
          <ApolloProvider client={client}>
              <App/>
          </ApolloProvider>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
reportWebVitals();
